
import PropTypes from 'prop-types'
import React from 'react'
import { FaFacebook, FaInstagram, FaTwitter } from 'react-icons/fa';

const Footer = ({ siteTitle, color, text  }) => (
  <div
    style={{
      background: color,
      height: 128,
      minHeight: '10vh',
    }}
  >
    <div
      style={{
        margin: '0 auto',
        maxWidth: 1200,
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
        flexWrap: 'wrap',
        height: 128,
        minHeight: '10vh',
        padding: '0 1.045rem',
        color: text
      }}>
      <div>
        <FaFacebook style={{padding: 8}} size={48}/>
        <FaInstagram style={{padding: 8}} size={48}/>
        <FaTwitter style={{padding: 8}} size={48}/>
      </div>
      <div style={{
        fontSize: '0.8rem',
      }}>Made with <a style={{color: text}} href="http://dopresence.com">presence()</a></div>
    </div>
  </div>
)

Footer.propTypes = {
  siteTitle: PropTypes.string,
}

Footer.defaultProps = {
  siteTitle: '',
}

export default Footer
