import React from 'react';

const SlideIndex = ({ slides, active, color, activeColor, onSlideChanged }) => (
    <div style={{
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        position: 'absolute',
        marginLeft: 'auto', 
        marginRight: 'auto',
        left: 0,
        right: 0,
        bottom: 24
    }}>
        {Array(slides).fill(0).map((v, index) => (
            <button key={index}
                onClick={() => onSlideChanged(index)}
                style={{
                    padding: '16px',
                    fontSize: 64,
                    backgroundColor: 'transparent',
                    border: 'none',
                    outline: 'none',
                    cursor: 'pointer',
                    color: active === index ? activeColor : color,
                    textShadow: `-1px -1px 0 ${active === index ? color : activeColor}, 
                        1px -1px 0 ${active === index ? color : activeColor}, 
                        -1px 1px 0 ${active === index ? color : activeColor},
                        1px 1px 0 ${active === index ? color : activeColor}`
                }}>.</button>
        ))}
    </div>
)

export default SlideIndex;