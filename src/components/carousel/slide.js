import React from 'react';

const Slide = ({image}) => (
    <div className="slide" 
        style={{
            width: '100%',
            height: '100%',
            display: 'inline-block',
            backgroundImage: `url(${image})`,
            backgroundSize: 'cover',
            backgroundRepeat: 'no-repeat',
            backgroundPosition: '50% 60%'
    }}></div>
)

export default Slide;