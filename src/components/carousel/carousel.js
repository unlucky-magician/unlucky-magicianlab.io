import React, { Component } from 'react';

import { StaticQuery, graphql } from 'gatsby';

import Slide from './slide';
import SlideIndex from './slide-index';

class Carousel extends Component {
    constructor(props) {
        super(props);

        this.state = {
            images: [
                "https://s3.us-east-2.amazonaws.com/dzuz14/thumbnails/aurora.jpg",
                "https://s3.us-east-2.amazonaws.com/dzuz14/thumbnails/canyon.jpg",
                "https://s3.us-east-2.amazonaws.com/dzuz14/thumbnails/city.jpg",
                "https://s3.us-east-2.amazonaws.com/dzuz14/thumbnails/desert.jpg",
                "https://s3.us-east-2.amazonaws.com/dzuz14/thumbnails/mountains.jpg",
                "https://s3.us-east-2.amazonaws.com/dzuz14/thumbnails/redsky.jpg",
                "https://s3.us-east-2.amazonaws.com/dzuz14/thumbnails/sandy-shores.jpg",
                "https://s3.us-east-2.amazonaws.com/dzuz14/thumbnails/tree-of-life.jpg"
            ],
            currentSlide: 0,
            translateValue: 0,
            interval: this.getInterval()
        };

        typeof window !== 'undefined' && window.addEventListener("resize", this.onResize.bind(this));
    }

    getInterval() {
        return typeof window !== 'undefined' && window.setInterval(() => {
            this.onSlideChanged(this.state.currentSlide + 1);
        }, 4000);
    }

    onSlideChanged(index) {
        if(index >= this.state.images.length) {
            index = 0;
        } else if (index < 0) {
            index = this.state.images.length;
        }
        typeof window !== 'undefined' && window.clearInterval(this.state.interval);
        this.setState(prevState => ({
            currentSlide: index,
            translateValue: prevState.translateValue + ((prevState.currentSlide - index) * this.slideWidth()),
            interval: this.getInterval()
        }));
    }

    onResize(event) {
        typeof window !== 'undefined' && window.clearInterval(this.state.interval);
        this.setState({
            currentSlide: 0,
            translateValue: 0,
            interval: this.getInterval()
        });
    }

    slideWidth = () => {
        return typeof window !== 'undefined' && window.innerWidth;
    }

    render() {
        return(<StaticQuery
            query={graphql`
              query {
                site {
                  siteMetadata {
                    title
                    colors {
                      primaryColor
                      primaryTextColor
                      secondaryColor
                      secondaryTextColor
                    }
                  }
                }
              }
            `}
            render={data => (
                <div style={{
                    position: 'relative',
                    width: '100%',
                    height: 600,
                    margin: '0 auto',
                    overflow: 'hidden',
                    whiteSpace: 'nowrap'
                }}>
                    <div style={{
                            position: 'relative',
                            width: '100%',
                            height: '100%',
                            transform: `translateX(${this.state.translateValue}px)`,
                            transition: 'transform ease-out 0.8s'
                        }}>
                        {
                            this.state.images.map((image, i) => (
                                <Slide key={i} image={image} />
                            ))
                        }
                    </div>
                    <SlideIndex slides={this.state.images.length} 
                        active={this.state.currentSlide} 
                        onSlideChanged={this.onSlideChanged.bind(this)}
                        color={data.site.siteMetadata.colors.primaryColor} 
                        activeColor={data.site.siteMetadata.colors.secondaryColor}/>
                </div>
            )}
          />
        )
    }
}

export default Carousel;