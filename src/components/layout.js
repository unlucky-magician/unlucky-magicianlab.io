import React from 'react'
import PropTypes from 'prop-types'
import { StaticQuery, graphql } from 'gatsby'

import Header from './header'
import Footer from './footer'
import './layout.css'

const Layout = ({ children }) => (
  <StaticQuery
    query={graphql`
      query SiteTitleQuery {
        site {
          siteMetadata {
            title
            colors {
              primaryColor
              primaryTextColor
              secondaryColor
              secondaryTextColor
            }
          }
        }
      }
    `}
    render={data => (
      <>
        <div style={{
          height: 6,
          backgroundColor: data.site.siteMetadata.colors.secondaryColor,
          width: '100%'
        }}></div>
        <Header siteTitle={data.site.siteMetadata.title} color={data.site.siteMetadata.colors.primaryColor} text={data.site.siteMetadata.colors.primaryTextColor}/>
        <div
          style={{
            margin: '0 auto',
            paddingTop: 0,
          }}
        >
          {children}
        </div>
        <Footer siteTitle={data.site.siteMetadata.title} color={data.site.siteMetadata.colors.primaryColor} text={data.site.siteMetadata.colors.primaryTextColor}/>
      </>
    )}
  />
)

Layout.propTypes = {
  children: PropTypes.node.isRequired,
}

export default Layout
