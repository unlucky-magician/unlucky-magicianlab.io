import React, { Component } from 'react';

class Hero extends Component {
    render() {
        return (<section style={{
            width: '100vw',
            height: '90vh',
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: this.props.color,
            backgroundImage: 'url("https://codetheweb.blog/assets/img/posts/full-image-hero/image.jpg")',
            backgroundSize: 'cover',
            backgroundPosition: 'center center',
            backgroundRepeat: 'no-repeat'
        }}>
            <div style={{

            }}>

            </div>
        </section>);
    }
}



export default Hero;