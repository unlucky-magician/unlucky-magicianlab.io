import React, { Component } from 'react';

import { StaticQuery, graphql } from 'gatsby'

import Friend from './friend';

class Friends extends Component {
    render() {
        return (<StaticQuery
            query={graphql`
              query {
                site {
                  siteMetadata {
                    title
                    colors {
                      primaryColor
                      primaryTextColor
                      secondaryColor
                      secondaryTextColor
                    }
                  }
                }
              }
            `}
            render={data => (<section style={{
                backgroundColor: data.site.siteMetadata.colors.secondaryColor,
                color: data.site.siteMetadata.colors.secondaryTextColor
            }}>
                <div style={{
                    margin: '0 auto',
                    maxWidth: 1200,
                    padding: '3.45rem 1.0875rem',
                }}>
                    <h1 style={{
                        textAlign: 'center'
                    }}>
                        Our Friends
                    </h1>
                    <div style={{
                        display: 'flex',
                        flexWrap: 'wrap',
                        justifyContent: 'space-around',
                        alignItems: 'center'
                    }}>
                     <Friend url='http://www.saltystudiosaus.com/' image='https://static1.squarespace.com/static/5752c6ac37013b03916b351a/t/5b34decc0e2e7239ad7eeb7e/1530191566793/16002950_1184984154882293_1951943344205803402_n+-+Copy.png?format=1000w'></Friend>
                     <Friend url='http://letsmakegames.org/' image='http://www.perthgamesfestival.com.au/wp-content/uploads/2015/07/lmg_full_large.png'></Friend>
                    </div>
                </div>
            </section>)}
          />)
    }
}

export default Friends;