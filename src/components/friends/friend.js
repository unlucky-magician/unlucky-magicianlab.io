import React, { Component } from 'react';

class Friend extends Component {
    render() {
        return (<a href={this.props.url}>
            <img 
                src={this.props.image} 
                alt=""
                style={{
                    maxWidth: 350,
                    maxHeight: 350
                }}/>
        </a>);
    }
}

export default Friend;