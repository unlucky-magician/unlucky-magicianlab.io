import React, { Component } from 'react';

class Profile extends Component {
    render() {
        const image = 'https://via.placeholder.com/150';
        return (<div style={{
            flex: '1 1 500px',
            display: 'flex',
            alignItems: 'center',
            padding: '1.45rem 0',
            maxWidth: 600
        }}>
            <div style={{
                flex: '1 1 auto',    
            }}>
                <img 
                    src={image} 
                    alt=""
                    style={{
                        borderRadius: '50%'
                    }}/>
            </div>
            <div style={{
                flex: '3 1 400px',
                padding: '0 1.0875rem',
            }}>
                <h1>Name</h1>
                <h4>Title</h4>
                <p>This is the description of the person, what they do. They also do this and like these games.
                    Game 1, Game 2, Game 3, and more text.
                </p>
            </div>
        </div>);
    }
}

export default Profile;