import React, { Component } from 'react';

import addToMailchimp from 'gatsby-plugin-mailchimp'

class MailingList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: '',
            submitResult: null
        }
    }

    async handleSubmit(event) {
        event.preventDefault();
        const result = await addToMailchimp(this.state.email, {});
        this.setState({
            submitResult: result
        });
    }

    onEmailChanged(event) {
        this.setState({
            email: event.target.value
        });
    }

    render() {
        return (<div style={{
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
            justifyContent: 'center'
        }}>
            <h2 style={{
                textAlign: 'center',
                lineHeight: 1.5,
                marginBottom: '0.8rem'
            }}>Subscribe</h2>
            <h5 style={{
                textAlign: 'center',
                lineHeight: 1.5
            }}>We would love to show you what we are doing!</h5>
            { this.state.submitResult ? <div dangerouslySetInnerHTML={this.state.submitResult.msg}></div>
            :
            <form onSubmit={this.handleSubmit.bind(this)}
                style={{
                    display: 'flex',
                    alignItems: 'center',
                    justifyContent: 'center',
                    flexWrap: 'wrap'
                }}>
                <input
                    style={{
                        flex: '0 0 auto',
                        width: 300,
                        color: this.props.color,
                        textDecoration: 'none',
                        padding: '8px 16px',
                        margin: '0.1rem 0.1rem',
                        fontWeight: 600,
                        border: '2px solid ' + this.props.color,
                        borderRadius: 8, 
                        backgroundColor: 'white'
                    }}
                    placeholder="Email Address"
                    type="email" 
                    name="email" 
                    id="email" 
                    value={this.state.email}
                    onChange={this.onEmailChanged.bind(this)}
                    required></input>
                <button
                    style={{
                        flex: '0 0 auto',
                        maxWidth: 200,
                        color: this.props.color,
                        textDecoration: 'none',
                        padding: '8px 16px',
                        margin: '0.1rem 0.1rem',
                        fontWeight: 600,
                        border: '2px solid ' + this.props.color,
                        borderRadius: 8, 
                        textTransform: 'uppercase',
                        backgroundColor: 'transparent'
                    }} 
                    type="submit">Subscribe</button>
            </form>}
        </div>)
    }
}

export default MailingList;