import React, { Component } from 'react';
import { StaticQuery, graphql } from 'gatsby'

import Profile from './profile/profile';
import MailingList from './mailing-list/mailing-list';

class About extends Component {
    render() {
        return(<StaticQuery
            query={graphql`
              query {
                site {
                  siteMetadata {
                    title
                    colors {
                      primaryColor
                      primaryTextColor
                      secondaryColor
                      secondaryTextColor
                    }
                  }
                }
              }
            `}
            render={data => (
                <section style={{
                    backgroundColor: data.site.siteMetadata.colors.secondaryColor,
                    color: data.site.siteMetadata.colors.secondaryTextColor,
                }}>
                    <div style={{
                        margin: '0 auto',
                        maxWidth: 1200,
                        padding: '3.45rem 1.0875rem',
                    }}>
                        <h1 style={{
                            textAlign: 'center',
                            lineHeight: 1.5
                        }}>
                            This is an about me section, there are many like it but this one is mine.
                        </h1>
                        <div style={{
                            display: 'flex',
                            flexWrap: 'wrap',
                            justifyContent: 'space-around',
                            alignItems: 'baseline',
                            paddingBottom: '3rem'
                        }}>
                            <Profile></Profile>
                            <Profile></Profile>
                        </div>
                        <div>
                            <MailingList color={data.site.siteMetadata.colors.primaryColor}/>
                        </div>
                    </div>
                </section>
            )}
          />
        )
    }
}

export default About;