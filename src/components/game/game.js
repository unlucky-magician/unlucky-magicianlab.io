import React, { Component } from 'react';

class Game extends Component {
    render() {
        const image = 'https://via.placeholder.com/400x200';
        return (<section style={{
            width: '100vw',
            height: '500px',
            display: 'flex',
            justifyContent: 'space-between',
            alignItems: 'center',
            backgroundImage: 'url("https://via.placeholder.com/1920x500")',
            backgroundSize: 'cover',
            backgroundPosition: 'center center',
            backgroundRepeat: 'no-repeat',
            flexWrap: 'wrap',
            textAlign: 'center'
        }}>
            <div style={{
                flex: '1 1 400px'
            }}>
                <img 
                    src={image} 
                    alt=""/>
            </div>
            <div style={{
                flex: '1 1 400px'
            }}>
                This is something about the game.
            </div>
            <div style={{
                flex: '1 1 400px'
            }}>
                Game Links
            </div>
        </section>);
    }
}

export default Game;