import React, { Component } from 'react';
import { Link } from 'gatsby';

class PostExcerpt extends Component {
    render() {
        return (
            <div style={{
                flex: '1 1 300px',
                display: 'flex',
                flexDirection: 'column',
                margin: '2rem',
                maxWidth: 300
            }}>
                <h6 style={{
                    textTransform: 'uppercase',
                    color: '#fff',
                    marginBottom: '0.25rem'
                }}>{this.props.post.frontmatter.date}</h6>
                <Link 
                    to={'/blog/' + this.props.post.fields.slug}
                    style={{
                        textDecoration: 'none',                    
                        fontWeight: 600,
                        color: this.props.secondaryColor
                    }}>
                    <h2 style={{
                        marginBottom: '0.5rem'
                    }}>{this.props.post.frontmatter.title}</h2>
                </Link>
                <h5 style={{
                    textTransform: 'uppercase',
                    marginBottom: '0.5rem'
                }}>{this.props.post.frontmatter.author}</h5>
                <p style={{
                    lineHeight: 1.8,
                    textAlign: 'justify'
                }}>
                    {this.props.post.excerpt}
                    <Link 
                        to={this.props.post.fields.slug}
                        style={{
                            textDecoration: 'none',                    
                            fontWeight: 400,
                            color: this.props.secondaryColor
                        }}>[Read More]</Link>
                </p>
            </div>
        )
    }
}

export default PostExcerpt;