import React, { Component } from 'react';

import { StaticQuery, graphql, Link } from 'gatsby'

import PostExcerpt from './post-excerpt';

class Blog extends Component {
    render() {
        return (<StaticQuery
            query={graphql`
              query {
                site {
                  siteMetadata {
                    title
                    colors {
                      primaryColor
                      primaryTextColor
                      secondaryColor
                      secondaryTextColor
                    }
                  }
                }
              }
            `}
            render={data => (this.props.posts.edges && this.props.posts.edges.length > 0 ? (<section style={{
                    backgroundColor: data.site.siteMetadata.colors.primaryColor,
                    color: data.site.siteMetadata.colors.primaryTextColor,
                    display: 'flex',
                    justifyContent: 'center',
                    alignItems: 'center',
                    flexDirection: 'column',
                    padding: '3.45rem 0'
                }}>
                    <h1 style={{
                        textAlign: 'center'
                    }}>Our Blog</h1>
                    <div style={{
                        margin: '0 auto',
                        maxWidth: 1200,
                        padding: '0 1.0875rem',
                        display: 'flex',
                        flexWrap: 'wrap',
                        alignItems: 'baseline',
                        justifyContent: 'space-around',              
                    }}>
                        {this.props.posts.edges.map(({ node }, i) => (
                            <PostExcerpt key={i} secondaryColor={data.site.siteMetadata.colors.secondaryColor} post={node}></PostExcerpt>   
                        ))}
                    </div>
                    <Link
                        style={{
                        flex: '0 0 auto',
                        maxWidth: 200,
                        color: data.site.siteMetadata.colors.secondaryColor,
                        textDecoration: 'none',
                        padding: '8px 16px',
                        margin: '0rem 0.5rem',
                        fontWeight: 600,
                        border: '2px solid ' + data.site.siteMetadata.colors.secondaryColor,
                        borderRadius: 8, 
                        textTransform: 'uppercase'
                        }}
                        to="/blog/">View All Posts</Link>
                </section>) : <></>)}
          />)
    }
}

export default Blog;

