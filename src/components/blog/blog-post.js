import React from 'react';
import Img from 'gatsby-image';
import { Link } from 'gatsby';

function BlogPost({post, primaryColor, secondaryColor}) {
    const { title } = post.frontmatter;
    const convertToKebabCase = (string) => {
        return string.replace(/\s+/g, '-').toLowerCase();
    }
    return (
        <>
            <Img fluid={post.frontmatter.image.childImageSharp.fluid} />
            <div style={{
                margin: '0 auto',
                maxWidth: 1200,
                padding: '3.45rem 0',
                lineHeight: 1.8
            }}>
                <h6 style={{
                    textTransform: 'uppercase',
                    marginBottom: '1rem',
                }}>{post.frontmatter.date}</h6>
                <Link 
                    to={'/blog/' + post.fields.slug}
                    style={{
                        textDecoration: 'none',                    
                        fontWeight: 600,
                    }}>
                    <h1 style={{
                        marginBottom: '1rem',
                        color: primaryColor
                    }}>{title}</h1>
                </Link>
                <h5 style={{
                    textTransform: 'uppercase',
                    marginBottom: '0.5rem'
                }}>By {post.frontmatter.author}</h5>
                <div dangerouslySetInnerHTML={{ __html: post.html }} />
                <h6 style={{
                    fontWeight: 600,
                    textAlign: 'right'
                }}>
                    Tags: 
                    {
                        post.frontmatter.tags.map((tag, index) => (
                            <Link 
                                key={tag}
                                style={{
                                    textDecoration: 'none',                    
                                    fontWeight: 600,
                                    color: secondaryColor
                                }}
                                to={"/tags/"+convertToKebabCase(tag)}> {tag}{index !== post.frontmatter.tags.length - 1 ? ',' : ''}</Link>
                        ))
                    }
                </h6>
            </div>
        </>
    );
}

export default BlogPost;