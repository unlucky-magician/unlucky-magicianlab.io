import { Link } from 'gatsby'
import PropTypes from 'prop-types'
import React from 'react'

const Header = ({ siteTitle, color, text }) => (
  <nav
    style={{
      background: color,
      height: 64,
      minHeight: '10vh',
    }}
  >
    <div
      style={{
        margin: '0 auto',
        maxWidth: 1200,
        display: 'flex',
        alignItems: 'center',
        flexWrap: 'wrap',
        height: 64,
        minHeight: '10vh',
        padding: '0 1.045rem',
      }}
    >
      <h1
        style={{
          marginRight: 'auto',
          marginBottom: 0,
          flex: '0 0 auto',
        }}
      >
        <Link
          to="/"
          style={{
            display: 'flex',
            alignItems: 'center',
          }}
        >
          <img
            style={{
              marginBottom: 0,
            }}
            src={'https://via.placeholder.com/64'}
            alt="logo"
          />
        </Link>
      </h1>
      {/* <Link
        style={{
          flex: '0 0 auto',
          color: text,
          textDecoration: 'none',
          padding: '0 0 2px 0',
          margin: '0rem 0.5rem',
          fontWeight: 600,
          borderBottom: '2px solid ' + text,
          textTransform: 'uppercase'
        }}
        to="/blog/">Blog</Link>
      <Link 
        style={{
          flex: '0 0 auto',
          color: text,
          textDecoration: 'none',
          padding: '0 0 2px 0',
          margin: '0rem 0.5rem',
          fontWeight: 600,
          borderBottom: '2px solid ' + text,
          textTransform: 'uppercase'
        }} 
        to="/presskit/">Presskit</Link> */}
    </div>
  </nav>
)

Header.propTypes = {
  siteTitle: PropTypes.string,
}

Header.defaultProps = {
  siteTitle: '',
}

export default Header
