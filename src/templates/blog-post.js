import React from 'react';
import Layout from '../components/layout';
import { graphql, Link } from 'gatsby'
import Img from 'gatsby-image';

import Metatags from '../components/metatags';

function BlogPost(props) {
    const post = props.data.markdownRemark;
    const { title, description } = post.frontmatter;
    const thumbnail = post.frontmatter.image.childImageSharp.resize.src
    const convertToKebabCase = (string) => {
        return string.replace(/\s+/g, '-').toLowerCase();
    }
    return (
        <Layout>
            <Metatags
                title={title}
                description={description}
                pathname={props.location.pathname}
            />
            <Img fluid={post.frontmatter.image.childImageSharp.fluid} />
            <div style={{
                margin: '0 auto',
                maxWidth: 1200,
                padding: '3.45rem 0rem',
                lineHeight: 1.8
            }}>
                <h6 style={{
                    textTransform: 'uppercase',
                    marginBottom: '1rem',
                }}>{post.frontmatter.date}</h6>
                <h1>{title}</h1>
                <h5 style={{
                    textTransform: 'uppercase',
                    marginBottom: '0.5rem'
                }}>By {post.frontmatter.author}</h5>
                <div dangerouslySetInnerHTML={{ __html: post.html }} />
                <h6 style={{
                    fontWeight: 600,
                    textAlign: 'right'
                }}>
                    Tags: 
                    {
                        post.frontmatter.tags.map((tag, index) => (
                            <Link 
                                style={{
                                    textDecoration: 'none',                    
                                    fontWeight: 600,
                                    color: props.data.site.siteMetadata.colors.secondaryColor
                                }}
                                to={"/tags/"+convertToKebabCase(tag)}> {tag}{index !== post.frontmatter.tags.length - 1 ? ',' : ''}</Link>
                        ))
                    }
                </h6>
            </div>
        </Layout>
    );
}

export default BlogPost;

export const query = graphql`
 query PostQuery($slug: String!) {
     markdownRemark(fields: { slug: { eq: $slug } }) {
       html
       frontmatter {
        date(formatString: "MMMM Do YYYY")
        title
        author
        tags
        image {
          childImageSharp {
            resize(width: 1500, height: 1500) {
              src
            }
            fluid(maxWidth: 786) {
              ...GatsbyImageSharpFluid
            }
          }
        }
       }
    }
    site {
        siteMetadata {
            colors {
                primaryColor
                primaryTextColor
                secondaryColor
                secondaryTextColor
              }
        }
    }
}
`