import React from "react"
import PropTypes from "prop-types"
import Helmet from 'react-helmet'

// Components
import { graphql } from "gatsby"

import Layout from '../components/layout'
import SEO from '../components/seo'

import BlogPost from '../components/blog/blog-post';

const Tags = ({ pageContext, data }) => {
  const { tag } = pageContext
  const { totalCount } = data.allMarkdownRemark
  const tagHeader = `${totalCount} Post${
    totalCount === 1 ? "" : "s"
  } Tagged "${tag}"`

  return (
    <Layout>
        <SEO title="Blog" />
        <Helmet title={`Tag - ${tag}`}/>
        <div style={{
            margin: '0 auto',
            maxWidth: 1200,
            padding: '3.45rem 1.0875rem',
        }}>
            <h1>{tagHeader}</h1>
            <div style={{
                display: 'flex',
                flexWrap: 'wrap-reverse',
                justifyContent: 'space-evenly',
            }}>
                <div style={{
                    flex: '3 1 800px',
                    padding: '1rem 1rem'
                }}>
                    {
                        data.allMarkdownRemark.edges.map(post => (
                            <BlogPost post={post.node} 
                            primaryColor={data.site.siteMetadata.colors.primaryColor} 
                            secondaryColor={data.site.siteMetadata.colors.secondaryColor}/>
                        ))
                    }
                </div>
            </div>
        </div>
    </Layout>
  )
}

Tags.propTypes = {
  pageContext: PropTypes.shape({
    tag: PropTypes.string.isRequired,
  }),
  data: PropTypes.shape({
    allMarkdownRemark: PropTypes.shape({
      totalCount: PropTypes.number.isRequired,
      edges: PropTypes.arrayOf(
        PropTypes.shape({
          node: PropTypes.shape({
            frontmatter: PropTypes.shape({
              title: PropTypes.string.isRequired,
            }),
          }),
        }).isRequired
      ),
    }),
  }),
}

export default Tags

export const pageQuery = graphql`
  query($tag: String) {
    allMarkdownRemark(
      limit: 2000
      sort: { fields: [frontmatter___date], order: DESC }
      filter: { frontmatter: { tags: { in: [$tag] } } }
    ) {
      totalCount
      edges {
        node {
            fields{
                slug
              }
              html
              frontmatter {
                date(formatString: "MMMM Do YYYY")
                title
                author
                tags
                image {
                  childImageSharp {
                    resize(width: 1500, height: 1500) {
                      src
                    }
                    fluid(maxWidth: 786) {
                      ...GatsbyImageSharpFluid
                    }
                  }
                } 
            }
        }
      }
    }
    site {
        siteMetadata {
          colors {
            primaryColor
            primaryTextColor
            secondaryColor
            secondaryTextColor
          }
        }
      }
  }
`