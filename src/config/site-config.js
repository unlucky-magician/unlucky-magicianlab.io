module.exports = {
    title: "Game Publisher Template",
    description: "Set up a game publisher/studio site with ease.",
    author: 'Tim Veletta',
    logoIcon: './src/images/logo-icon-small.png',

    colors: {
        primaryColor: '#2f3640',
        primaryTextColor: '#ffffff',
        secondaryColor: '#c23616',
        secondaryTextColor: '#ecf0f1'
    },

    about: {
        main: '',
        
    }
    
}