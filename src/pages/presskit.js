import React from 'react'

import './presskit.css';

import Layout from '../components/layout'
import SEO from '../components/seo'

const Presskit = () => (
  <Layout>
    <SEO title="Presskit" />
    <div style={{
      display: 'flex',
      padding: '3rem 3rem',
      flexWrap: 'wrap',
      lineHeight: 1.8
    }}>
      <div style={{
        flex: '1 1 300px'
      }}>
        <h1>Unlucky Magician</h1>
        <h4 style={{
          marginBottom: '3rem'
        }}>
          <a href="http://www.unluckymagician.com">http://www.unluckymagician.com</a>
        </h4>
        <h2>Fact Sheet</h2>

        <div className="fact">
          <label>Founding Date</label>
          <div>September 1, 2018</div>
        </div>

        <div className="fact">
          <label>Based In</label>
          <div>Launceston, Australia</div>
        </div>

        <div className="fact">
          <label>Press / Business Contact</label>
          <div>
            <a href="mailto: hello@unluckymagician.com">hello@unluckymagician.com</a>
          </div>
        </div>

        <div className="fact">
          <label>Social</label>
          <div>
            <a href="https://twitter.com/unluckymagician">twitter.com/unluckymagician</a>
          </div>
          <div>
            <a href="https://facebook.com/unluckymagician">facebook.com/unluckymagician</a>
          </div>
          <div>
            <a href="https://instagram.com/unluckymagician">instagram.com/unluckymagician</a>
          </div>
        </div>

        <div className="fact">
          <label>Releases</label>
          <div>
            <a href="https://teleblastgame.com">TeleBlast</a>
          </div>
        </div>

        <div className="fact">
          <label>Address</label>
          <div>
            This is an Address
          </div>
          <div>
            It occurs over multiple lines
          </div>
          <div>
            This is line 3
          </div>
        </div>

        <div className="fact">
          <label>Phone Number</label>
          <div>
            This is an Address
          </div>
        </div>

      </div>
      <div style={{
        flex: '2 3 600px'
      }}>
        <img src="https://placehold.it/1920x600" alt="header"/>

        <h2>Description</h2>
        <p>Unlucky Magician is a games collaboration between Chris Baron and Tim Veletta with the aim of building fun, accessible and social games to connect people.</p>

        <h2>History</h2>

        <h2>Awards and Recognition</h2>

        <h2>Projects</h2>

        <h2>Videos</h2>

        <h2>Pictures</h2>

        <h2>Logo</h2>

        <h2>Articles</h2>

        <h2>Additional Links</h2>

        <div style={{
          display: 'flex',
        }}>
          <div style={{
            flex: '0 1 50%'
          }}>
            <h2>The Team</h2>
          </div>
          <div style={{
            flex: '0 1 50%'
          }}>
            <h2>Contact</h2>
          </div>
        </div>
        
      </div>
    </div>
  </Layout>
)

export default Presskit
