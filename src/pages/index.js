import React from 'react'

import { graphql } from 'gatsby'

import Layout from '../components/layout'
import SEO from '../components/seo'
import Hero from '../components/hero'
import About from '../components/about/about'
import Game from '../components/game/game'
import Blog from '../components/blog/blog'
import Friends from '../components/friends/friends'
import Carousel from '../components/carousel/carousel'

const IndexPage = props => {
  return (
    <Layout>
      <SEO title="Home" keywords={['gatsby', 'application', 'react']} />
      <Hero />
      <About />
      {/* <Game></Game>
      <Game></Game>
      <Blog posts={props.data.allMarkdownRemark}></Blog>
      <Carousel></Carousel>
      <Friends></Friends> */}
    </Layout>
  )
}

export default IndexPage

export const listQuery = graphql`
  query ListQuery {
    allMarkdownRemark(
      limit: 3
      sort: { order: DESC, fields: [frontmatter___date] }
    ) {
      edges {
        node {
          fields {
            slug
          }
          excerpt(pruneLength: 300)
          frontmatter {
            date(formatString: "MMMM Do YYYY")
            title
            author
          }
        }
      }
    }
  }
`
