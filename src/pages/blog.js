import React from 'react'

import { Link, graphql } from 'gatsby';

import Layout from '../components/layout'
import SEO from '../components/seo'

import BlogPost from '../components/blog/blog-post';

const Blog = ({data}) => {
  const convertToKebabCase = (string) => {
    return string.replace(/\s+/g, '-').toLowerCase();
  }
  return (<Layout>
      <SEO title="Blog" />
      <div style={{
          margin: '0 auto',
          maxWidth: 1200,
          padding: '3.45rem 1.0875rem',
      }}>
        <div style={{
          display: 'flex',
          flexWrap: 'wrap-reverse',
          justifyContent: 'space-evenly',
        }}>
          <div style={{
          flex: '3 1 800px',
          padding: '1rem 1rem'
        }}>
        {
          data.allMarkdownRemark.edges.map((post, index) => (
            <BlogPost key={index} post={post.node} 
              primaryColor={data.site.siteMetadata.colors.primaryColor} 
              secondaryColor={data.site.siteMetadata.colors.secondaryColor}/>
          ))
        }
        </div>
          <div style={{
            flex: '1 1 200px',
            padding: '1rem 1rem'
          }}>
            <h3>Tags</h3>
            <ul style={{
              listStyleType: 'none',
              padding: 0,
              margin: 0
            }}>
              {
                data.allMarkdownRemark.group.map(tag => (
                  <li key={tag.fieldValue}>
                    <Link 
                      key={tag.fieldValue}
                      style={{
                        color: data.site.siteMetadata.colors.secondaryColor,
                        textDecoration: 'none',                    
                        fontWeight: 400,
                      }}
                      to={"/tags/"+convertToKebabCase(tag.fieldValue)}>{tag.fieldValue} ({tag.totalCount})</Link>
                  </li>   
                ))
              }
            </ul>
          </div>
      </div>
      </div>
    </Layout>
  );
}

export default Blog

export const blogQuery = graphql`
  query BlogQuery {
    allMarkdownRemark(sort: { order: DESC, fields: [frontmatter___date] }) {
      group(field: frontmatter___tags) {
        fieldValue
        totalCount
      }
      edges {
        node {
          fields{
            slug
          }
          html
          frontmatter {
            date(formatString: "MMMM Do YYYY")
            title
            author
            tags
            image {
              childImageSharp {
                resize(width: 1500, height: 1500) {
                  src
                }
                fluid(maxWidth: 786) {
                  ...GatsbyImageSharpFluid
                }
              }
            }  
          }
        }
      }
    }
    site {
      siteMetadata {
        colors {
          primaryColor
          primaryTextColor
          secondaryColor
          secondaryTextColor
        }
      }
    }
  }
`