const path = require("path")
const { createFilePath, createFileNode } = require(`gatsby-source-filesystem`)
exports.createPages = ({ actions, graphql }) => {
    const { createPage } = actions
    const blogPostTemplate = path.resolve(`src/templates/blog-post.js`)
    const tagTemplate = path.resolve("src/templates/tags.js")
    return new Promise((resolve, reject) => {
        resolve(graphql(`
    {
      allMarkdownRemark(
        sort: { order: DESC, fields: [frontmatter___date] }
        limit: 1000
      ) {
        edges {
          node {
              fields{
                  slug
              }
            frontmatter {
              title
              tags
            }
          }
        }
      }
    }
  `).then(result => {
                if (result.errors) {
                    console.log(result.errors)
                    return reject(result.errors)
                }
                let tags = [];
                result.data.allMarkdownRemark.edges.forEach(({ node }) => {
                    tags = tags.concat(node.frontmatter.tags);
                    createPage({
                        path: `/blog${node.fields.slug}`,
                        component: blogPostTemplate,
                        context: {
                            slug: node.fields.slug,
                        }, // additional data can be passed via context
                    })
                })
                tags = tags.filter((value, index, self) => {
                    return self.indexOf(value) === index;
                });
                tags.forEach(tag => {
                    createPage({
                        path: `/tags/${convertToKebabCase(tag)}/`,
                        component: tagTemplate,
                        context: {
                            tag,
                        },
                    });
                })
                return
            })
        )
    })
}

exports.onCreateNode = ({ node, getNode, actions }) => {
    const { createNodeField } = actions
    if (node.internal.type === `MarkdownRemark`) {
        const slug = createFilePath({ node, getNode, basePath: `pages` })
        createNodeField({
            node,
            name: `slug`,
            value: slug,
        })
    }
}


const convertToKebabCase = (string) => {
    return string.replace(/\s+/g, '-').toLowerCase();
}